const Promise = require('bluebird');
const { promisify } = Promise;
const puppeteer = require('puppeteer');
const rp = require('request-promise');
const fs = require('fs');
const { promises: fsp } = fs;
const { exec } = require('child_process');
const ep = promisify(exec);

async function main() {
  const browser = await puppeteer.launch({args: ['--no-sandbox', '--disable-setuid-sandbox']});
  const page = await browser.newPage();

  await mkdir('download');

  for (let i = 0; i < process.argv.length - 2; ++i) {
    const input = process.argv[2 + i];

    const rePattern = new RegExp(/^https?:\/\/app\.crowdmark\.com/);
    let url;

    if (input.match(rePattern)) {
      url = input;
    } else {
      url = 'https://app.crowdmark.com/score/' + input;
    }

    console.log("url: ", url);

    const res = await scrape(page, url);
    const path = `download/${res.title}`;
    await mkdir(path);

    console.log('Started', `${res.title}:`,
      `${res.pages.length} page${(res.pages.length == 1) ? '' : 's'}`);

    await Promise.each(res.pages,
      async (p, i) => {
        const pPath = `${path}/${i}`;
        await mkdir(pPath);

        await mkdir(`${pPath}/annot`);
        await mkdir(`${pPath}/annot/svgs`);
        await mkdir(`${pPath}/annot/anchors`);

        await Promise.all([
          (async u => fsp.writeFile(`${pPath}/img.jpg`, await rp(u, { encoding : null })))(p.background),
          Promise.each(p.svgs,
            (ff, j) => fsp.writeFile(`${pPath}/annot/svgs/svg${j}.svg`, ff)),
          Promise.each(p.anchors,
            (a, j) => {
              const label = `${a.name} (${a.score.toString().trim()})`;
              return ep(`magick -pointsize 32 label:"${label}" -border 10 \
                -fill "#5ab873d0" -draw 'color 0,0 reset' -gravity center -fill white \
                -annotate 0 "${label}" "${pPath}/annot/anchors/anchor${j}.png"`);
            })
        ]);

        const [width, height] = (await ep(`identify -format %wx%h "${pPath}/img.jpg"`)).split('x');
        let cmd = `magick -background none "${pPath}/img.jpg" `;
        p.svgs.forEach((_, j) =>
          cmd += `"${pPath}/annot/svgs/svg${j}.svg" -geometry ${width}x${height} -composite `);
        cmd += `"${pPath}/out.png"`;
        await ep(cmd);

        if (p.anchors.length > 0) {
          cmd = `magick "${pPath}/out.png" `;
          p.anchors.forEach((a, j) =>
            cmd += `"${pPath}/annot/anchors/anchor${j}.png" -geometry \
            +${parseFloat(a.pos.left)*width/100}+${parseFloat(a.pos.top)*height/100} \
            -composite `
          );
          cmd += `"${pPath}/out.png"`;

          await ep(cmd);
        }

        console.log('Completed', res.title, `page ${i + 1}`);
      });

    let cmd = 'magick ';
    for (let i = 0; i < res.pages.length; ++i) {
      cmd += `${path}/${i}/out.png `;
    }
    cmd += `download/${res.title}.pdf`;
    await ep(cmd);
    console.log("Completed", res.title);
  }

  await browser.close();
}

async function mkdir(dir) {
  if (!fs.existsSync(dir))
    await fsp.mkdir(dir);
}

async function scrape(page, url) {
  await page.goto(url);
  await page.waitFor(1000);

  const result = page.evaluate(() => {
    const title = document.querySelector('header.main-header h1 strong').innerText.split(' ').join('_');

    const score = document.querySelector('.score-view__score').innerText;

    const pages = [].slice.apply(document.querySelectorAll('section.main-content .score-view__page'))
      .map(e => {
        const background = e.querySelector('img').src;
        const a = e.querySelector('.annotation-capture');

        const svgs = [].slice.apply(a.querySelectorAll('svg'))
          .filter(e => e.querySelectorAll('svg').length);

        svgs.forEach(e => {
          e.querySelectorAll('svg.freeform-annotation')
            .forEach(el => {
              el.style = "fill: none; stroke: rgb(64, 168, 195); stroke-width: 3; stroke-linecap: round; stroke-linejoin: round; opacity: 0.75;";
            });

          e.querySelectorAll('path.delete-path')
            .forEach(el => {
              el.style.opacity = 0;
            });

          e.querySelectorAll('svg.box-annotation')
          .forEach(el => {
            el.style = "fill: rgb(64, 168, 195); opacity: .4;";
          });
        });

        const anchors = [].slice.apply(e.querySelectorAll('.page__question-anchor'))
          .map(e => {
            const name = e.querySelector('.cm-label').innerText;
            const score = e.querySelector('.cm-points').innerText;

            return {
              pos: {
                left: e.style.left,
                top: e.style.top
              },
              name,
              score
            }
          });

        const comments = [].slice.apply(e.querySelectorAll('.cm-comment'))
          .map(e => ({
            pos: {
              left: e.style.left,
              top: e.style.top
            },
            text: e.innerText.split('\n').map(e => e.trim()).filter(e => e.length).join(' '),
          }));

        return {
          background,
          svgs: svgs.map(e => e.outerHTML),
          anchors,
          comments,
        }
      });

    return {
      title,
      score,
      pages,
    };
  })

  return result;
}

main();
