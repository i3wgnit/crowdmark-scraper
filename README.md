# Crowdmark Scraper
A simple program to download evaluations from Crowdmark and compile into a nice PDF.

# Setup

`npm install`

# Usage

- `node index.js https://app.crowdmark.com/score/00000000-0000-0000-0000-000000000000 [...]`
- `node index.js 00000000-0000-0000-0000-000000000000 [...]`

**Note**: you must use the shareable link.

# Dependencies

- NodeJS 10
- ImageMagick 7
- ghostscript

# TODO

- Add grader comments
